#!/bin/sh

qemu-system-x86_64 \
    -enable-kvm \
    -smp cores=2,threads=1,sockets=1 \
    -drive file=${HOME}/vm/ubuntu-18.04.img,if=virtio,format=raw \
    -netdev user,id=ubuntu-18.04,hostfwd=tcp::5555-:22 \
    -device virtio-net-pci,netdev=ubuntu-18.04 \
    -m 8G \
    -device intel-hda -device hda-duplex \
    -name ubutunu-18.04 \
    -vnc :1 \
    -monitor unix:${HOME}/vm/ctrl-ubuntu-18.04,server,nowait \
    "$@"
