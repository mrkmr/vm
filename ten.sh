#!/bin/sh

export QEMU_AUDIO_DRV=spice

qemu-system-x86_64 \
	-enable-kvm \
	-machine q35,kernel_irqchip=on,type=pc,accel=kvm \
	-cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time \
	-smp cores=2,threads=1,sockets=1 \
	-drive file=/home/manu/vm/images/ten.img,if=virtio,format=qcow2,cache=none \
	-netdev user,id=usernet \
	-device virtio-net-pci,netdev=usernet \
	-m 8G \
	-soundhw hda \
	-name windows \
	-vnc :1 \
	-vga qxl \
	-global PIIX4_PM.disable_s3=1 -global PIIX4_PM.disable_s4=1 \
	-device virtio-serial-pci \
	-monitor unix:/home/manu/vm/control/ten,server,nowait \
	-spice port=5958,addr=127.0.0.1,disable-ticketing \
	"$@"
	#-device vfio-pci,sysfsdev=/sys/class/mdev_bus/0000:00:02.0/ff55a4ad-54f2-4906-b749-64df48958c99,rombar=0 \
	#-net user,smb=/home/manu \
	#-usbdevice tablet \
