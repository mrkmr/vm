#!/bin/sh

qemu-system-x86_64 \
    -enable-kvm \
    -smp cores=2,threads=1,sockets=1 \
    -drive file=${HOME}/vm/debian-10.img,if=virtio,format=raw \
    -netdev user,id=debian,hostfwd=tcp::5555-:22 \
    -device virtio-net-pci,netdev=debian \
    -m 8G \
    -device intel-hda -device hda-duplex \
    -name debian \
    -vnc :1 \
    -monitor unix:${HOME}/vm/ctrl-debian,server,nowait \
    "$@"
